from django.conf.urls import  url

from . import views

app_name='produto'
urlpatterns = [
    url(r'^$', views.index, name='index'),
	url(r'^produto/cadastrar.html', views.cadastrar, name='cadastrar'),
	url(r'^produto/listar.html', views.listar, name='listar'),
	url(r'^produto/registrarCompra.html', views.registrarCompra, name='registrar compra'),
	url(r'^produto/compraRegistrada.html', views.listarRegistroCompras, name='compra registrada'),
	url(r'^produto/filaProcessamento.html', views.filaProcessamento, name='fila de processamento')

]
